const TXT_1 = 'Produced message: '
const TXT_2 = 'Successfully produced record to topic TOPIC partition PARTITION VALUE'
const TXT_3 = '\nConsumed message: '

module.exports = {
  TXT_1,
  TXT_2,
  TXT_3,
}