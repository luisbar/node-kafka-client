const CLIENT_CONFIG = {
  'bootstrap.servers': process.env.KAFKA_SERVER,
}

module.exports = {
  topic: 'transfer',
  clientConfig: CLIENT_CONFIG,
  producerConfig: {
    ...CLIENT_CONFIG,
    'dr_msg_cb': true,
  },
  consumerConfig: {
    ...CLIENT_CONFIG,
    'group.id': 'transfer-consumer',
  },
  offsetConfig: {
    'auto.offset.reset': 'earliest',
  },
  topicConfig: {
    topics: 'transfer',
    waitInterval: 0,
    objectMode: false,
  },
}