const path = require('path')

require('dotenv-safe').config({
  path: path.join(__dirname, '/config/.env'),
  example: path.join(__dirname, '/config/.env.example'),
})

const lineReader = require('./utils/lineReader')
const { TXT_1 } = require('./assets/strings')
const createTopic = require('./services/createTopic')
const produce = require('./services/produce')
const consume = require('./services/consume')

const onWriteMessage = (message) => {
  try {

    produce({
      value: Buffer.from(message),
      key: `KEY ${Date.now()}`
    })

    setTimeout(() => lineReader.question(TXT_1, onWriteMessage), 1)

  } catch (error) {
    console.log(error)
  }
}

const init = async () => {
  try {

    await createTopic()
    consume()

    lineReader.question(TXT_1, onWriteMessage)

  } catch (error) {
    console.log(error)
  }
}

init()



