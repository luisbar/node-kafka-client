const Kafka = require('node-rdkafka')
const config = require('../config/development')
const { TXT_3 } = require('../assets/strings')

module.exports = () => {
  const consumer = initializeConsumer()
  consumeData({ consumer })
}

const initializeConsumer = () => {
  const consumer = Kafka.KafkaConsumer.createReadStream(
    config.consumerConfig,
    config.offsetConfig,
    config.topicConfig
  )

  return consumer
}

const consumeData = ({ consumer }) => {
  consumer.on('ready', onReady({ consumer }))
  consumer.on('data', onData)
  consumer.on('error', onError)

  consumer.connect()
}

const onReady = ({ consumer }) => () => {
  consumer.subscribe([config.topic])
  consumer.consume()
}

const onError = (error) => {
  throw error
}

const onData = (data) => {
  const message = Buffer.from(data).toString('utf-8')
  console.log(`${TXT_3}${message}`)
}