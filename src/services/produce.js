const Kafka = require('node-rdkafka')

const config = require('../config/development')
const  { TXT_2 } = require('../assets/strings')

module.exports = ({ value, key }) => {
  const producer = initializeProducer()
  produceData({ producer, value, key })
}

const initializeProducer = () => {
  const producer = new Kafka.Producer(config.producerConfig)

  return producer
}

const produceData = ({ producer, value, key }) => {
  producer
  .on('ready', onReady({ producer, topic: config.topic, value, key }))
  .on('delivery-report', onDeliveryReport)
  .on('event.error', onError)

  producer.connect()
}

const onReady = ({ producer, topic, value, key }) => () => {
  producer.produce(topic, -1, value, key)
}

const onDeliveryReport = (error, report) => {
  if (error) throw error
  
  const { topic, partition, value } = report
  const message = TXT_2
  .replace('TOPIC', topic)
  .replace('PARTITION', partition)
  .replace('VALUE', value)

  console.log(message)
}

const onError = (error) => {
  throw error
}