const Kafka = require('node-rdkafka')
const config = require('../config/development')

const TOPIC_ALREADY_EXISTS = 36

module.exports = () => {
  const client = initializeClient()
  return createTopic({ client })
}

const initializeClient = () => {
  const client = Kafka.AdminClient.create(config.clientConfig)

  return client
}

const createTopic = ({ client }) => {
  return new Promise((resolve, reject) => {
    client.createTopic(
      {
        topic: config.topic,
        num_partitions: 3,
        replication_factor: 3,
      },
      onCreateTopic({ resolve, reject })
    )
  })
}

const onCreateTopic = ({ resolve, reject }) => (error) => {
  if (!error ||
      error.code === TOPIC_ALREADY_EXISTS)
    return resolve(true)  

  return reject(error.message)
}