## Run the project

#### Execute the following command
```bash
npm start
```

## Kafka

#### Key concepts

- Idempotent producer: An idempotent operation is one where if you perform it multiple times it has the same effect as if it were performed once

- Transaction: transactions enable atomic writes to multiple Kafka topics and partitions. All of the messages included in the transaction will be successfully written or none of them will be. For example, an error during processing can cause a transaction to be aborted, in which case none of the messages from the transaction will be readable by consumers

#### How to install kafka

- Download it from [here](https://kafka.apache.org/downloads)
- In the bin folder are all the scripts: zookeeper-server-start.sh, kafka-server-start.sh, etc

#### How to start kafka

- First you have to run zookeeper
```bash
zookeeper-server-start.sh ./zookeeper.properties
```

- By default zookeeper run in the port 2181, but you can change it in the zookeeper.properties

- Finally you have to run kafka
```bash
kafka-server-start.sh ./server.properties
```

- By default kafka run in the port 9092, but you can change it in the server.properties
- If you want to have more than once broker you have to run kafka-server-start.sh using a server.properties file with the following field modified:
```sh
broker.id=YOUR_BROKER_ID
listeners=PLAINTEXT://:YOUR_PORT
log.dirs=/PATH/YOUR_FOLDER
```

#### Topic commands

- Create a topic
```bash
kafka-topics.sh --zookeeper localhost:2181 --topic YOUR_TOPIC_NAME --create --partitions 3 --replication-factor 2
```

- List topics
```bash
kafka-topics.sh --zookeeper localhost:2181 --list
```

- Check detail about a topic
```bash
kafka-topics.sh --zookeeper localhost:2181 --topic YOUR_TOPIC_NAME --describe
```

- Delete a topic
```bash
kafka-topics.sh --zookeeper localhost:2181 --topic YOUR_TOPIC_NAME --delete
```

#### Consumer groups

- List consumer groups
```bash
kafka-consumer-groups.sh --bootstrap-server localhost:9091 --list
```

- Check consumer group
```bash
kafka-consumer-groups.sh --bootstrap-server localhost:9091 --group YOUR_GROUP --describe
```

#### Produce data

- Basic command
```bash
kafka-console-producer.sh --broker-list localhost:9091 \
--topic YOUR_TOPIC
```

- With key separator
```bash
kafka-console-producer.sh --broker-list localhost:9091 \
--topic YOUR_TOPIC \
--property "parse.key=true" \
--property "key.separator=:"
```

#### Consume data

- Basic command
```bash
kafka-console-consumer.sh --bootstrap-server localhost:9091 \
    --topic YOUR_TOPIC \
    --from-beginning
```

- Formatting the key and deserializating it
```bash
kafka-console-consumer.sh --bootstrap-server localhost:9091 \
    --topic YOUR_TOPIC \
    --from-beginning \
    --formatter kafka.tools.DefaultMessageFormatter \
    --property print.key=true \
    --property print.value=true \
    --property key.deserializer=org.apache.kafka.common.serialization.StringDeserializer \
    --property value.deserializer=org.apache.kafka.common.serialization.LongDeserializer
```

#### WordCound application (Java)

- Create the input topic
```bash
kafka-topics.sh --zookeeper localhost:2181 --topic input --create --partitions 3 --replication-factor 2
```

- Create the output topic
```bash
kafka-topics.sh --zookeeper localhost:2181 --topic output --create --partitions 3 --replication-factor 2
```

- Install libraries
```xml
<dependency>
  <groupId>org.apache.kafka</groupId>
  <artifactId>kafka-streams</artifactId>
  <version>2.4.1</version>
</dependency>
<dependency>
  <groupId>org.tuxdude.logback.extensions</groupId>
  <artifactId>logback-colorizer</artifactId>
  <version>1.0.1</version>
  <type>jar</type>
</dependency>
<dependency>
  <groupId>ch.qos.logback</groupId>
  <artifactId>logback-classic</artifactId>
  <version>1.2.3</version>
</dependency>
```

- Run following code
```java
logger.info("run:start");

Properties properties = new Properties();

properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "subsidy_consumer");
properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9091");
properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());

logger.info("run:properties");

StreamsBuilder builder = new StreamsBuilder();
KStream<String, String> stream = builder.stream("input");

logger.info("run:builder");

KTable<String, Long> dataProcessed = stream
.mapValues(value -> value.toLowerCase())
.flatMapValues(value -> Arrays.asList(value.split(" ")))
.selectKey((key, value) -> value)
.groupByKey()
.count(Materialized.as("Counts"));

logger.info("run:dataProcessed");

dataProcessed.toStream().to("output", Produced.with(Serdes.String(), Serdes.Long()));

KafkaStreams streams = new KafkaStreams(builder.build(), properties);
streams.start();

logger.info("run:streams:{}", streams.toString());

Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
```

- Run the consumer
```bash
kafka-console-consumer.sh --bootstrap-server localhost:9091 \
    --topic output \
    --from-beginning \
    --formatter kafka.tools.DefaultMessageFormatter \
    --property print.key=true \
    --property print.value=true \
    --property key.deserializer=org.apache.kafka.common.serialization.StringDeserializer \
    --property value.deserializer=org.apache.kafka.common.serialization.LongDeserializer
```

- Run the producer
```bash
kafka-console-producer.sh --broker-list localhost:9091 \
--topic input \
--property "parse.key=true" \
--property "key.separator=:"
```

- Write data

#### Kafka connector

- Download your connector from [here](https://www.confluent.io/hub)

- Put your connector in some folder e.g. /YOUR_PATH/connectors/debezium-debezium-connector-mysql-1.1.0

- Decide if you want to run an standalone or distributed connector, more information [here](https://docs.confluent.io/current/connect/userguide.html#connect-configuring-workers)

- Configure your standalone connector modifying the file called connect-standalone.properties, you have to modify the following properties
```properties
# Point to one broker of your kafka cluster
bootstrap.servers=localhost:9091
# You can put many directories separating it by commas
plugin.path=/YOUR_PATH/connectors/debezium-debezium-connector-mysql-1.1.0
```

- You have to create a file for configuring your connectors, more information [here](https://docs.confluent.io/current/connect/managing/configuring.html) e.g. debezium-debezium-connector-mysql.properties
```properties
name=NAME
connector.class=io.debezium.connector.mysql.MySqlConnector
database.hostname=HOST
database.port=3306
database.user=USER
database.password=PASSWORD
database.server.id=UNIQUE_ID
database.server.name=UNIQUE_NAME
database.whitelist=DATABASE_NAME
database.history.kafka.bootstrap.servers=localhost:9091
database.history.kafka.topic=YOUR_TOPIC
include.schema.changes=true
```

- You have to create your topic where data will be saved
```bash
kafka-topics.sh --zookeeper localhost:2181 --topic YOUR_TOPIC_NAME --create --partitions 3 --replication-factor 2
```

- Run a standalone connector
```bash
connect-standalone.sh connect-standalone.properties debelzium-mysql-cdc-connector.properties
```
